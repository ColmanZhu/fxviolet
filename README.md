# FXViolet
# Team Class Diagrams: 
Tian Peng: File load/save 

Colman Zhu: UI Transfer 

Daniel Chang: Change Class Structure 

Project Installation: https://www.eclipse.org/efxclipse/install.html#for-the-ambitious

Class Diagram:
 
![Scheme](violet/ClassDiagram.png)

#Week 1 Goals

* There is a functional specification document on the Wiki (describing what features you will be able to do, with a description of the user interface.)
* There are issues in the issue tracker. Each issue must be doable in ≤ 3 days and have a single person assigned to it. Each team member must have ≥ 3 issues.
* Some code is in Git and builds on the command line, including javadoc and CheckStyle
* Each team member submitted their individual reports

#Week 2 Goals

* Each team member has made at least two commits. (Note: You all commit to the main branch. No branching/merging.)
* Design documentation is complete (classes, maybe a sequence diagram or two if it's relevant for your project, I/O format or protocol if it is relevant)
* The issue tracker has enough issues to carry the project to the end of week 3. Issues are well distributed among team members, short and assigned to a single person.
* The project builds (on the command line) and does something useful. Put instructions for building and for trying out that useful feature on the wiki.
* Each team member has closed at least one issue and has put the closed issue number(s) in their report.
* Each team member submitted their individual report, with weekly and total hours.

#Week 3 Goals

* The project builds and does something significant. Put instructions for building and for trying out that significant feature on the wiki.
* A status page lists what already works and what doesn't yet work.
* There are sufficient issues to keep each team member busy for project completion.
* Each team member has made at least two commits.
* Each team member has closed at least two issues and has put the closed issue number(s) in their report.
* Each team member submitted their individual report, with weekly and total hours.

#Week 4 Goals

* Design documentation is revisited and lists all planned classes.
* All issues that are required for successful completion of the project are on the issue list.
* The status page is updated to list what doesn't yet work.
* The project builds and accomplishes most of what is required, perhaps with some errors.
* Every team member has multiple commits throughout the week.
* Keep your individual reports up as before.

#Week 5 Goals

* The implementation is complete.
* Every team member has multiple commits throughout the week.
* Keep the status page and your individual reports up as before.