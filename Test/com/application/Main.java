package application;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			CustomLayout root = new CustomLayout(new MenuBarEditor(), new Toolbar(), new Canvas());
			Scene scene = new Scene(root,600,600);
			primaryStage.setTitle("ClassDiagramEditor");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
