package application;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.VBox;

public class CustomLayout extends VBox{
	
	public CustomLayout(MenuBar menubar, Toolbar toolbar, Canvas canvas){
		super();
		this.menubar = menubar;
		this.toolbar = toolbar;
		this.canvas = canvas;
		setLayOut();
	}
	
	private void setLayOut() {
		getChildren().add(menubar);
		getChildren().add(toolbar);
		getChildren().add(canvas);
	}
	
	public Toolbar getToolBar(){
		return toolbar;
	}
	
	public Canvas getCanvas(){
		return canvas;
	}

	private MenuBar menubar;
	private Toolbar toolbar;
	private Canvas canvas;
}
