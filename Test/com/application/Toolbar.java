package application;

import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Toolbar extends StackPane{
	public Toolbar(){
		setAlignment(Pos.BASELINE_CENTER);
		group = new ToggleGroup();
	    tools = new ArrayList<>();
	    ToggleButton grabberButton = new ToggleButton();
	    grabberButton.setMinSize(25.0, 25.0);
	    grabberButton.setMaxSize(25.0, 25.0);
	    Polygon grabberImage = new Polygon(0, 0,grabberButton.getMinWidth()/2, 0,0, grabberButton.getMinHeight()/2);
	    grabberImage.setFill(Color.PURPLE);
	    grabberButton.setGraphic(grabberImage);
	    grabberButton.setToggleGroup(group);
	    grabberButton.setSelected(true);
	    tools.add(null);
	    getChildren().add(grabberButton);
	    /*
	    Node[] nodeTypes = graph.getNodePrototypes();
	      for (Node n : nodeTypes)
	         add(n);
	      Edge[] edgeTypes = graph.getEdgePrototypes();
	      for (Edge e : edgeTypes)
	         add(e);
	     */
	}
	/*
	public void add(final Node n){
		ToggleButton button = new ToggleButton();
		button.setGraphic(n.getNode());
		button.setToggleGroup(group);
		tools.add(n);
		getChildren().add(n.getNode());
	}
	
	 public void add(final Edge e) {
		ToggleButton button = new ToggleButton();
		
		button.setGraphic(e.getConnectionPoints());
		button.setToggleGroup(group);
		tools.add(e);
		getChildren().add(e.getConnectionPoints());
	   }
	*/
	private ToggleGroup group;
	private ArrayList<Object> tools;
}
