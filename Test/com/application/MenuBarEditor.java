package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MenuBarEditor extends MenuBar{
	public MenuBarEditor(){
		super();
		setMenuBar();
	}

	private void setMenuBar() {
		Menu menuFile = new Menu("File");
		Menu menuEdit = new Menu("Edit");
		Menu menuView = new Menu("View");
		Menu menuHelp = new Menu("Help");
		MenuItem fileSave = new MenuItem("Save");
		MenuItem fileImport = new MenuItem("Import");
		MenuItem fileExport = new MenuItem("Export");
		MenuItem helpReadMe = new MenuItem("Readme");
		MenuItem fileExit = new MenuItem("Exit");
		MenuItem editDelete = new MenuItem("Delete");
		MenuItem editProperties = new MenuItem("Properties");
		MenuItem editNext = new MenuItem("Next");
		MenuItem editPrevious = new MenuItem("Previous");
		MenuItem viewZoomOut = new MenuItem("ZoomOut");
		MenuItem viewZoomIn = new MenuItem("ZoomIn");
		MenuItem viewgrowDrawingArea = new MenuItem("GrowDrawingArea");
		MenuItem viewclipDrawingArea = new MenuItem("ClipDrawingArea");
		MenuItem viewsmallerGrid = new MenuItem("SmallerGrid");
		MenuItem viewlargerGrid = new MenuItem("LargerGrid");
		fileSave.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		fileImport.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		fileExport.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		fileExit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				System.exit(0);
			}
		});
		menuFile.getItems().addAll(fileSave, fileImport, fileExport, fileExit);
		editProperties.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		editDelete.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		editNext.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		editPrevious.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		menuEdit.getItems().addAll(editDelete, editProperties,editNext,editPrevious);
		viewZoomOut.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		viewZoomIn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		viewgrowDrawingArea.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		viewclipDrawingArea.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		viewsmallerGrid.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		viewlargerGrid.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		menuView.getItems().addAll(viewZoomOut, viewZoomIn, viewgrowDrawingArea, viewclipDrawingArea, viewsmallerGrid, viewlargerGrid);
		helpReadMe.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e){
				//to be implemented
			}
		});
		menuHelp.getItems().addAll(helpReadMe);
		getMenus().addAll(menuFile,menuEdit,menuView,menuHelp);
	}
}
