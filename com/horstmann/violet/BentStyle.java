/*
Violet - A program for editing UML diagrams.

Copyright (C) 2002 Cay S. Horstmann (http://horstmann.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package com.horstmann.violet;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import com.horstmann.violet.framework.SerializableEnumeration;

import javafx.scene.shape.Rectangle;

/**
   A style for a segmented line that indicates the number
   and sequence of bends.
*/
public class BentStyle extends SerializableEnumeration
{
   private BentStyle() {}
  
   /**
      Gets the points at which a line joining two rectangles
      is bent according to this bent style.
      @param rectangle the starting rectangle
      @param rectangle2 the ending rectangle
      @return an array list of points at which to bend the
      segmented line joining the two rectangles
   */
   public ArrayList getPath(Rectangle rectangle, Rectangle rectangle2)
   {
      ArrayList r = getPath(this, rectangle, rectangle2);
      if (r != null) return r;
      
      if (rectangle.equals(rectangle2)) r = getSelfPath(rectangle);
      else if (this == HVH) r = getPath(VHV, rectangle, rectangle2);
      else if (this == VHV) r = getPath(HVH, rectangle, rectangle2);
      else if (this == HV) r = getPath(VH, rectangle, rectangle2);
      else if (this == VH) r = getPath(HV, rectangle, rectangle2);
      if (r != null) return r;

      return getPath(STRAIGHT, rectangle, rectangle2);
   }

   /**
      Gets the four connecting points at which a bent line
      connects to a rectangle.
   */
   private static Point2D[] connectionPoints(Rectangle rectangle)
   {
      Point2D[] a = new Point2D[4];
      //a[0] = new Point2D.Double(rectangle.getX(), rectangle.getCenterY());
      //a[1] = new Point2D.Double(rectangle.getMaxX(), rectangle.getCenterY());
      //a[2] = new Point2D.Double(rectangle.getCenterX(), rectangle.getY());
      //a[3] = new Point2D.Double(rectangle.getCenterX(), rectangle.getMaxY());
      a[0] = new Point2D.Double(rectangle.getX(), rectangle.getHeight()/2 + rectangle.getY() + rectangle.getHeight()/2);
      a[1] = new Point2D.Double(rectangle.getWidth() + + rectangle.getX(), rectangle.getHeight()/2 + rectangle.getY());
      a[2] = new Point2D.Double(rectangle.getWidth()/2 + rectangle.getX(), rectangle.getY());
      a[3] = new Point2D.Double(rectangle.getWidth()/2 + rectangle.getX(), rectangle.getHeight() + + rectangle.getY());
      return a;
   }
   
   /**
      Gets the points at which a line joining two rectangles
      is bent according to a bent style.
      @param start the starting rectangle
      @param end the ending rectangle
      @return an array list of points at which to bend the
      segmented line joining the two rectangles
   */
   private static ArrayList getPath(BentStyle bent, 
      Rectangle rectangle, Rectangle rectangle2)
   {
      ArrayList r = new ArrayList();
      if (bent == STRAIGHT)
      {
         Point2D[] a = connectionPoints(rectangle);
         Point2D[] b = connectionPoints(rectangle2);
         Point2D p = a[0];
         Point2D q = b[0];
         double distance = p.distance(q);
         if (distance == 0) return null;
         for (int i = 0; i < a.length; i++)
            for (int j = 0; j < b.length; j++)
            {
               double d = a[i].distance(b[j]);
               if (d < distance)
               {
                  p = a[i]; q = b[j];
                  distance = d;
               }
            }
         r.add(p);
         r.add(q);
      }
      else if (bent == HV)
      {
         double x1;
         //double x2 = rectangle2.getCenterX();
         //double y1 = rectangle.getCenterY();
         double x2 = rectangle2.getWidth()/2;
         double y1 = rectangle.getHeight()/2;
         double y2;
         if (x2 + MIN_SEGMENT <= rectangle.getX())
            x1 = rectangle.getX();
         else if (x2 - MIN_SEGMENT >= rectangle.getWidth() + rectangle.getX())
            x1 = rectangle.getWidth() + rectangle.getX();
         else return null;
         if (y1 + MIN_SEGMENT <= rectangle2.getY())
            y2 = rectangle2.getY();
         else if (y1 - MIN_SEGMENT >= rectangle2.getHeight() + rectangle2.getY())
            y2 = rectangle2.getHeight() + rectangle2.getY();
         else return null;
         r.add(new Point2D.Double(x1, y1));
         r.add(new Point2D.Double(x2, y1));
         r.add(new Point2D.Double(x2, y2));
      }
      else if (bent == VH)
      {
         //double x1 = rectangle.getCenterX();
    	 double x1 = rectangle.getWidth()/2 + rectangle.getX();
         double x2;
         double y1;
         //double y2 = rectangle2.getCenterY();
         double y2 = rectangle2.getHeight()/2 + rectangle2.getY();
         if (x1 + MIN_SEGMENT <= rectangle2.getX())
            x2 = rectangle2.getX();
         else if (x1 - MIN_SEGMENT >= rectangle2.getWidth() + rectangle2.getX())
            x2 = rectangle2.getWidth() + rectangle2.getX();
         else return null;
         if (y2 + MIN_SEGMENT <= rectangle.getY())
            y1 = rectangle.getY();
         else if (y2 - MIN_SEGMENT >= rectangle.getHeight() + rectangle.getY())
            //y1 = rectangle.getMaxY();
        	 y1 = rectangle.getHeight() + rectangle.getY();
         else return null;
         r.add(new Point2D.Double(x1, y1));
         r.add(new Point2D.Double(x1, y2));
         r.add(new Point2D.Double(x2, y2));
      }
      else if (bent == HVH)
      {
         double x1;
         double x2;
         //double y1 = rectangle.getCenterY();
         double y1 = rectangle.getHeight()/2 + rectangle.getY();
         //double y2 = rectangle2.getCenterY();
         double y2 = rectangle2.getHeight()/2 + rectangle2.getY();
         if (rectangle.getWidth() + rectangle.getX() + 2 * MIN_SEGMENT <= rectangle2.getX())
         {
            x1 = rectangle.getWidth() + rectangle.getX();
            x2 = rectangle2.getX();
         }
         else if (rectangle2.getWidth() + rectangle2.getX() + 2 * MIN_SEGMENT <= rectangle.getX())
         {
            x1 = rectangle.getX();
            x2 = rectangle2.getWidth() + rectangle2.getX();
         }
         else return null;
         if (Math.abs(y1 - y2) <= MIN_SEGMENT)
         {
            r.add(new Point2D.Double(x1, y2));
            r.add(new Point2D.Double(x2, y2));
         }
         else
         {
            r.add(new Point2D.Double(x1, y1));
            r.add(new Point2D.Double((x1 + x2) / 2, y1));
            r.add(new Point2D.Double((x1 + x2) / 2, y2));
            r.add(new Point2D.Double(x2, y2));
         }
      }
      else if (bent == VHV)
      {
         //double x1 = rectangle.getCenterX();
         //double x2 = rectangle2.getCenterX();
    	 double x1 = rectangle.getWidth()/2 + rectangle.getX();
    	 double x2 = rectangle2.getWidth()/2 + rectangle2.getX();
         double y1;
         double y2;
         if (rectangle.getHeight() + rectangle.getY() + 2 * MIN_SEGMENT <= rectangle2.getY())
         {
            y1 = rectangle.getHeight() + rectangle.getY();
            y2 = rectangle2.getY();
         }
         else if (rectangle2.getHeight() + rectangle2.getY() + 2 * MIN_SEGMENT <= rectangle.getY())
         {
            y1 = rectangle.getY();
            y2 = rectangle2.getHeight() + rectangle2.getY();

         }
         else return null;
         if (Math.abs(x1 - x2) <= MIN_SEGMENT)
         {
            r.add(new Point2D.Double(x2, y1));
            r.add(new Point2D.Double(x2, y2));
         }
         else
         {
            r.add(new Point2D.Double(x1, y1));
            r.add(new Point2D.Double(x1, (y1 + y2) / 2));
            r.add(new Point2D.Double(x2, (y1 + y2) / 2));
            r.add(new Point2D.Double(x2, y2));
         }
      }
      else return null;
      return r;
   }

   /**
      Gets the points at which a line joining two rectangles
      is bent according to a bent style.
      @param rectangle the starting and ending rectangle
   */
   private static ArrayList getSelfPath(Rectangle rectangle)
   {
      ArrayList r = new ArrayList();
      double x1 = rectangle.getX() + rectangle.getWidth() * 3 / 4;
      double y1 = rectangle.getY();
      double y2 = rectangle.getY() - SELF_HEIGHT;
      double x2 = rectangle.getX() + rectangle.getWidth() + SELF_WIDTH;
      double y3 = rectangle.getY() + rectangle.getHeight() / 4;
      double x3 = rectangle.getX() + rectangle.getWidth();
      r.add(new Point2D.Double(x1, y1));
      r.add(new Point2D.Double(x1, y2));
      r.add(new Point2D.Double(x2, y2));
      r.add(new Point2D.Double(x2, y3));
      r.add(new Point2D.Double(x3, y3));
      return r;
   }

   private static final int MIN_SEGMENT = 10;
   private static final int SELF_WIDTH = 30;
   private static final int SELF_HEIGHT = 25;

   public static final BentStyle STRAIGHT = new BentStyle();
   public static final BentStyle HV = new BentStyle();
   public static final BentStyle VH = new BentStyle();
   public static final BentStyle HVH = new BentStyle();
   public static final BentStyle VHV = new BentStyle();
}
